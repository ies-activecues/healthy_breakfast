#include <iostream>

#include "encoder.h"

using namespace std;
namespace hb = healthy_breakfast;

string 
vector_to_string(vector<string> v) {
    string s;
    for(int i=0; i < v.size(); i++) {
        s += v[i];
        if(i != v.size() - 1)
            s += ", ";
    }
    return s;
}

int main(int argc, char * argv[]) {

    hb::encoder enc;

    cout << enc.to_binary(argv[1]) << endl;
    cout << enc.to_hex(argv[1]) << endl;
    cout << vector_to_string( enc.to_fruit(argv[1]) ) << endl;

    return 0;
}

