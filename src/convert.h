#pragma once

#include <map>

namespace healthy_breakfast {
    static const std::map<char,std::string> fruit_map = {
        {'0', "grapes"},
        {'1', "melon"},
        {'2', "watermelon"},
        {'3', "tangerine"},
        {'4', "lemon"},
        {'5', "banana"},
        {'6', "pineapple"},
        {'7', "mango"},
        {'8', "apple"},
        {'9', "pen"},
        {'a', "pear"},
        {'b', "peach"},
        {'c', "cherries"},
        {'d', "strawberry"},
        {'e', "kiwi"},
        {'f', "coconut"}
    };

    static const std::map<std::string,char> hex_map = {
        { "0000", '0' },
        { "0001", '1' },
        { "0010", '2' },
        { "0011", '3' },
        { "0100", '4' },
        { "0101", '5' },
        { "0110", '6' },
        { "0111", '7' },
        { "1000", '8' },
        { "1001", '9' },
        { "1010", 'a' },
        { "1011", 'b' },
        { "1100", 'c' },
        { "1101", 'd' },
        { "1110", 'e' },
        { "1111", 'f' }
    };
}
