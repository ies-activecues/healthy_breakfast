#include "encoder.h"

#include <sstream>
#include <cstring>
#include <iostream>

#include "convert.h"

using namespace std; 
using namespace healthy_breakfast; 

string
encoder::to_binary(const string& serial) 
{
    stringstream finalString;
    string yetToConvert = serial;
    string partial;

    // Beamer Type
    partial = yetToConvert.substr(0, 1);
    finalString << convertBeamerTypeToBit(partial);
    yetToConvert = yetToConvert.substr( partial.size(), yetToConvert.size());

    // Hardware Version
    partial = yetToConvert.substr(0, 2);
    finalString << convertHardwareVersionToBit(partial);
    yetToConvert = yetToConvert.substr( partial.size(), yetToConvert.size());

    // Flavour
    partial = extractFlavourFromSerial(yetToConvert);
    int flavourcode_size = partial.size();
    finalString << convertFlavourToBit(partial);
    yetToConvert = yetToConvert.substr( partial.size(), yetToConvert.size());

    // Year Number
    partial = yetToConvert.substr(0, 2);
    finalString << convertYearNumberToBit(partial);
    yetToConvert = yetToConvert.substr(partial.size(), yetToConvert.size());

    // Remove E BECAUSE IT'S FIXED VALUE
    partial = yetToConvert.substr(0, 1);
    yetToConvert = yetToConvert.substr( partial.size(), yetToConvert.size());

    // Unit Number
    partial = yetToConvert.substr( 0, 6 - flavourcode_size );
    finalString << convertUnitNumberToBit(partial);
    yetToConvert = yetToConvert.substr( partial.size(), yetToConvert.size());

    // ActiveCues PostFix
    partial = yetToConvert.substr( 0, partial.size() );
    finalString << convertPostFixToBit(partial);

    return finalString.str();
}

string
encoder::to_hex(const string& serial) 
{
    string bitstring = to_binary( serial );
    stringstream ss;
    while(bitstring.size() > 3) {
        //cout << "bits left: " << bitstring.size() << endl;
        ss << hex_map.at( bitstring.substr(0, 4) );
        bitstring = bitstring.substr(4, bitstring.size());
    }
    return ss.str();
}

vector<string>
encoder::to_fruit(const string& serial) 
{
    string hex = to_hex(serial);

    vector<string> v;

    for(int i=0; i< hex.size(); i++) {
        v.push_back( fruit_map.at( hex[i] ) );
    }

    return v;
}

int
encoder::convertPostFixToBit(const string& postFix)
{
    return postFix.size() > 0 ? 1 : 0;
}

string
encoder::convertUnitNumberToBit(const string& unitNumber)
{
    int n = atoi(unitNumber.c_str());
    return int_to_binary(n,10);
}

string
encoder::convertYearNumberToBit(const string& yearNumber)
{
    int n = atoi(yearNumber.c_str());
    return int_to_binary(n - 10,5);
}

string
encoder::convertFlavourToBit(const string& flavour)
{
    if (flavour.find("HUA",0) != false) 
        return "001";
    if (flavour.find("HSA",0) != false) 
        return "010";
    if (flavour.find("HA",0) != false) 
        return "000";
    return "";
}

string
encoder::extractFlavourFromSerial(const string& serialNumber)
{
    if (serialNumber.find("HUA",0) != false) 
        return "HUA";
    if (serialNumber.find("HSA",0) != false) 
        return "HSA";
    if (serialNumber.find("HA",0) != false) 
        return "HA";
    return "";
}

string
encoder::convertHardwareVersionToBit(const string& hardwareVersion)
{
    int hw = atoi(hardwareVersion.c_str());
    return int_to_binary(hw - 7,4);
} 

int
encoder::convertBeamerTypeToBit(const string& beamer) 
{
    return (beamer == "M") ? 0 : 1;
}
